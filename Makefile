.PHONY: www

clean:
	rm www/bundle.js

install:
	npm install
	bower install

www:
	@browserify www/js/entry.js -o www/bundle.js
	@cordova prepare

android: www
	cordova build android

test: www
	protractor test/protractor.conf.js
