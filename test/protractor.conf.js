exports.config = {
	// A base URL for your application under test. Calls to protractor.get()
  // with relative paths will be prepended with this.
  //baseUrl: 'http://localhost:8000/browser/www/',

  // ---- 2. To connect to a Selenium Server which is already running ----------
  // The address of a running Selenium Server. If specified, Protractor will
  // connect to an already running instance of Selenium.
  seleniumAddress: 'http://localhost:4444/wd/hub',


  // Spec patterns are relative to the location of this config.
  // Use karma runner to run backoffice scenarios
  specs: [
    './**/*.scenario.js',
  ],
  // The params object will be passed directly to the Protractor instance,
  // and can be accessed from your test as browser.params. It is an arbitrary
  // object and can contain anything you may need in your test.
  // This can be changed via the command line as:
  //   --params.login.user 'Joe'
  // params: {
  // },

  // Alternatively, suites may be used. When run without a command line
  // parameter, all suites will run. If run with --suite=smoke or
  // --suite=smoke,full only the patterns matched by the specified suites will
  // run.
  // suites: {
  //   full: 'spec/*.js',
  // },

  // Test framework to use. This may be jasmine, jasmine2, cucumber, or mocha.
  framework:'mocha',

  mochaOpts:{
    reporter:'spec',
    slow:3000,
    enableTimeouts: false
  },

  capabilities:{
    'browserName':'chrome'
  }
};
