var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var expect = chai.expect;


describe('Cordova application scenario', function(){


  beforeEach(function () {
    browser.get('http://localhost:8000/browser/www/')
  });

  describe('Central logo', function(){
      var p = element(by.css('p.event.received'));

      it('should blink if devicerady was fired', function() {
        expect(p.getText()).to.eventually.equal("DEVICE IS READY");
      });
  });

});
