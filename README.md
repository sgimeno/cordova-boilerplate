cordova-boilerplate
===================

Starter for Cordova apps

###Setup

```
>npm install -g cordova bower browserify
>git clone git@github.com:sgimeno/cordova-boilerplate.git
>cd cordova-boilerplate
>cordova platform add <android|ios|browser|...>
>make android
```

###Make commands

 + install
 + www
 + android
 + test

TODO:

 + asset pipeline: gulp + browserify + cordova prepare + gapreload? - I+D
 + .direnv config (APP_NAME, ICON, LANGUAGE)
 + read plugins from config.xml in hooks
 + tests on the browser - DONE
 + appium tests
