#!/usr/bin/env node

var exec = require('child_process').exec;

// make sure there are no plugins installed, usefull when Platform SDK upgrades, etc.
exec("cordova plugin ls | cut -d ' ' -f 1 | xargs -n1 cordova plugin rm");
