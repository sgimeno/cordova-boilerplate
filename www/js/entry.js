var angular = require('angular');

angular.module('demo', [
  require('angular-material')
])
.controller('DemoCtrl', DemoCtrl);

function DemoCtrl ($element) {


  // ******************************
  // Internal methods
  // ******************************

  function listen(){
    document.addEventListener("deviceready", function () {
      var ps = $element.find('p');

      var listeningElement = ps[0];
      var receivedElement = ps[1];

      listeningElement.setAttribute('style', 'display:none;');
      receivedElement.setAttribute('style', 'display:block;');

    }, false);
  }

  //blink on device ready
  listen();
}
